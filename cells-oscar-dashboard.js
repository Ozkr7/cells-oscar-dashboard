{
  const {
    html,
  } = Polymer;
  /**
    `<cells-oscar-dashboard>` Description.

    Example:

    ```html
    <cells-oscar-dashboard></cells-oscar-dashboard>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-oscar-dashboard | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsOscarDashboard extends Polymer.Element {

    static get is() {
      return 'cells-oscar-dashboard';
    }

    static get properties() {
      return {
        conditional: {
          type: Boolean,
          value: true
        },
        lista: {
          type: Object,
          value: { },
          notify: true
        }, prop2: {
          type: String,
          notify: true,
          value: '',
        }, poke: {
          type: Object,
          notify: true,
          value: { }
        }
      };
    }
    ajaxResponse(e) {
      this.poke = e.detail.response;
      console.log(e.detail.response);
    }
    filtro(item) {
      console.log('si entra');
      console.log(item);
      return item.name.toLowerCase() === this.prop2.toLowerCase();
    }
    refiltro() {
      console.log('si entra3');
      console.log(Polymer.dom(this.root).querySelector('#users'));
      Polymer.dom(this.root).querySelector('#users').render();
    }

    static get template() {
      return html `
      <style include="cells-oscar-dashboard-styles cells-oscar-dashboard-shared-styles"></style>
      <slot></slot>

          
          <iron-ajax
            auto
              url="https://pokeapi.co/api/v2/pokemon/salamence/"
              handle-as="json"
              on-response="ajaxResponse"></iron-ajax>




          <div class="card">
            <template is="dom-if" if="{{!conditional}}" >
              <div class="card">
                <login-oscar  prop4={{conditional}} ></login-oscar>
              </div>

            </template>



          <template is="dom-if" if="{{conditional}}" >
            <div class="card2" >
              <p>{{poke.name}}</p>
              <p><img src="{{poke.sprites.front_shiny}}" alt=""> <img src="{{poke.sprites.back_shiny}}" alt=""></p>
              <p><img src="{{poke.sprites.front_default}}" alt=""><img src="{{poke.sprites.back_default}}" alt=""></p>
              <p>Filtro<input class="sisi" type="text" value="{{prop2::input}}" on-keyup="refiltro" ></p>
            </div>
              <br>



                  <cells-mocks-component alumnos="{{lista}}"></cells-mocks-component>
                  <div class="card2">
                    <template is="dom-repeat" items="{{lista}}" filter="filtro" id="users">
                      <div class="card" >
                          <div class="card3">
                                <img src="{{item.img}}" alt="notienesuimagen.jpg">
                          </div>
                          <div class="card3">
                            <div>Nombre: <span>{{item.name}}</span></div>
                            <div>Hobbies: <span>{{item.hobbies}}</span></div>
                          </div>


                      </div>

                    </template>
                  </div>




          </template>
        </div>





      `;
    }
  }

  customElements.define(CellsOscarDashboard.is, CellsOscarDashboard);
}
